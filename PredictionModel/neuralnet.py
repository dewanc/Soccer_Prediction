import tensorflow as tf
import csv
import random
import pandas as pd
from sklearn import metrics, model_selection
import numpy as np
import matplotlib.pyplot as plt
from Util.db import DbHelper
from Data.data import Match
from BaseModel.model import BaseModel



class NeuralNet(BaseModel):


    def __init__(self):
        # extend base class
        self.path_to_saved_model = "/tmp/tensor_saved_models/v2"
        self.metafile_name = "my_modal"
        path_of_checkpoint = tf.train.latest_checkpoint(checkpoint_dir="/tmp/tensor_saved_models/v2")

        # check if there is checkpoint created in past
        if path_of_checkpoint:
            # set the saved flag to True
            self.FLAG_SAVED = True
            # print("saved found")
        else:
            self.FLAG_SAVED = False
            # print("Not found")
        BaseModel.__init__(self)


    def conti(self,x):
        if x == '1':
            return np.asarray([1, 0, 0])
        elif x == '2':
            return np.asarray([0,1, 0])
        else:
            return np.asarray([0, 0, 1])

    def __format_input(self, data, testing_split):
        """
        It will format the given data into dataframe and split them into training and testing
        data as per the split ratio given.
        :param data:
        :param testing_split:
        :return:
        """

        # here the data is changed to pandas dataframe. dataframe contains features list
        # well as the target class
        header_list = ["home_api","away_api" ]
        header_list.extend(self.feature_list)
        header_list.append(self.target_class_name)

        df1 = pd.DataFrame(data, columns=header_list)

        df1 = df1.iloc[np.random.permutation(len(df1))]

        # split the data to train and test data with the given split percentage
        df_train, df_test = model_selection.train_test_split(df1, test_size=testing_split, random_state=42)

        # self.train_x = df_train.as_matrix(self.feature_list)
        temp = df_train.as_matrix(self.feature_list)
        # self.train_x = [float(i) for i in temp]

        self.train_x = [[float(j) for j in i] for i in temp]

        # self.test_x = df_test.as_matrix(self.feature_list)
        temp1 = df_test.as_matrix(self.feature_list)
        # self.test_x = [float(i) for i in temp1]
        self.test_x = [[float(j) for j in i] for i in temp1]

        # target class
        target_class = self.target_class_name

        mod_train_y = df_train.as_matrix([target_class])
        mod_test_y = df_test.as_matrix([target_class])

        # self.train_y = [i[0] for i in mod_train_y]
        #tempty = [i[0] for i in mod_train_y]
        self.train_y = [self.conti(i) for i in mod_train_y]

        # self.test_y = [i[0] for i in mod_test_y]
        #temptxy = [i[0] for i in mod_test_y]
        self.test_y = [self.conti(i) for i in mod_test_y]


    def model_graph(self):
        """
        This function has the main model algorithm used. It will set the structure of error_value and
        the train step needed while training.
        set: x = feature set placeholder
             y_ = actual target class
             y = predicted target class
             error_value = error value function top optimize
             train_step = the optimizer used
        :return:
        """
        self.target_class_order = 3

        # make placeholder for the input training data. It will be feed later inside the session.
        self.tensor_feature = tf.placeholder(tf.float32, [None, self.features_num], name="Input_data")


        # make a vaiable for the weights used. It will change while error reduction.
        self.weights = tf.Variable(tf.zeros([self.features_num, self.target_class_order]), name="Weights")

        # make a varianble for the bias. It will change while error reduction.
        self.bias = tf.Variable(tf.zeros([self.target_class_order]), name="Bias")


        # The main model function is written for class prediction.
        self.tensor_predicted_class = tf.nn.softmax(tf.matmul(self.tensor_feature, self.weights) + self.bias)

        # make a placeholder for the actual target class. It will feed later inside session.
        self.tensor_actual_class = tf.placeholder(tf.float32, [None, self.target_class_order],name="actual_target_class")

        # set the error fuction.
        with tf.name_scope('Error'):
            cross_entropy = tf.reduce_mean(
                -tf.reduce_sum(self.tensor_actual_class * tf.log(self.tensor_predicted_class), reduction_indices=[1]))
        error_func = cross_entropy
        tf.summary.scalar('Error', cross_entropy)

        # set the train step
        learning_rate = 0.0001
        self.train_step = tf.train.AdamOptimizer(learning_rate).minimize(error_func)


    def train(self,testing_split):
        """
        train calls all dependecies and trains the model.
        :return: 
        """

        with open('english.csv', 'rb') as f:
            reader = csv.reader(f)
            train_data = list(reader)

        data = train_data



        self.__format_input(data, testing_split)
        self.model_graph()

        self.features_num = len(self.feature_list)
        # start a session for tensorflow.
        sess = tf.InteractiveSession()

        # initialize all global variables.
        tf.global_variables_initializer().run()
        # check if the model was saved before.
        if self.FLAG_SAVED:
            # if there is a saved model load the variables
            saver = tf.train.Saver()
            saver.restore(sess, save_path=self.path_to_saved_model + "/" + self.metafile_name)

        correct_prediction = tf.equal(tf.argmax(self.tensor_predicted_class, 1), tf.argmax(self.tensor_actual_class, 1))
        with tf.name_scope('accuracy'):
            accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

        # make summary scaler for accuracy
        tf.summary.scalar('Accuracy', accuracy)

        # group all scaler at one variable so feeding data becomes easy
        merged = tf.summary.merge_all()

        # get a writer to write the summary to the disk
        writer = tf.summary.FileWriter("/tmp/tensor_saved_models/v2",
                                       sess.graph)
        # make a saver to save once new model is trained
        saver = tf.train.Saver()

        # run the model inside the training loop.
        for i in range(2000):
            # train the model and reduce the error.
            sess.run(self.train_step, feed_dict={self.tensor_feature: self.train_x,
                                                 self.tensor_actual_class: np.array((self.train_y), dtype=int)})
            # print(sess.run(self.tensor_predicted_class,feed_dict={self.tensor_feature: self.train_x}))
            print(
                sess.run(accuracy, feed_dict={self.tensor_feature: self.test_x, self.tensor_actual_class: self.test_y}))

            print(sess.run(self.tensor_predicted_class, feed_dict={self.tensor_feature: self.train_x}))
            # after each 10 epoc we write the scaler to the disk
            if i % 10 == 0:
                summary = sess.run(merged, feed_dict={self.tensor_feature: self.train_x,
                                                      self.tensor_actual_class: self.train_y})
                writer.add_summary(summary, i)
        # saving the variable of model for later use
        saver.save(sess, save_path=self.path_to_saved_model + "/" + self.metafile_name)

        # print(sess.run(self.y, {self.x: [[5.6, 3.0, 4.5, 1.5]]}))



        print
        "Accuracy:"
        print(sess.run(accuracy, feed_dict={self.tensor_feature: self.test_x, self.tensor_actual_class: self.test_y}))

        self.FLAG_SAVED = True



    def predict_match(self, test_row):


        # get match object
        # tf.reset_default_graph()
        # match_row = DbHelper.get_match(match_id)
        # match = Match(match_row[0])
        features= test_row
        # features = self.get_training_features(match)
        # print features
        training_x= [features[i] for i in range(2,15)]
        # print training_x
        if not self.FLAG_SAVED:
            print("Model Not Train. Please run train function.")
            return

        tf.reset_default_graph()
        self.model_graph()
        sess = tf.InteractiveSession()
        tf.global_variables_initializer().run()
        # check if the model was saved before.
        if self.FLAG_SAVED:
            # if there is a saved model load the variables


            saver = tf.train.Saver()
            saver.restore(sess, save_path=self.path_to_saved_model + "/" + self.metafile_name)
            # print (sess.run(self.tensor_predicted_class, {self.tensor_feature: [training_x]}))
            temp= (sess.run(self.tensor_predicted_class, {self.tensor_feature: [training_x]}))
            win = temp[0][0]
            draw = temp[0][1]
            loss= temp[0][2]

            print("result for test_row")
            if win>draw and win>loss:
                print "home_win"
            if draw>win and draw>loss:
                print "draw"
            if loss>win and loss>draw:
                print "away_win"




