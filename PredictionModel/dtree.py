from BaseModel.model import BaseModel
from sklearn import tree,model_selection
import pandas as pd
import numpy as np
import csv
from sklearn.metrics import accuracy_score
from IPython.display import Image
import pydotplus
from sklearn.externals.six import StringIO
from sklearn.metrics import confusion_matrix
from Explore.explore import *


class DecisionTree(BaseModel):
    """
    DecisionTree class uses decision tree to predict the target class.
    """

    def __int__(self):


        BaseModel.__init__(self)
        self.target_names = ["home_win", "draw", "away_win"]



    def conti(self,x):
        if x == '1':
            return "home_win"
        elif x == '2':
            return "draw"
        else:
            return "away_win"

    def __format_input(self, data, testing_split):
        """
        It will format the given data into dataframe and split them into training and testing
        data as per the split ratio given.
        :param data:
        :param testing_split:
        :return:
        """
        new_feature_list = ["home_attack", "home_defence", "home_mid", "away_attack", "away_defence", "away_mid",
                            "home_last_clash_wins",
                            "away_last_clash_wins",
                            "last_clash_draw"]
        # here the data is changed to pandas dataframe. dataframe contains features list
        # well as the target class
        header_list = ["home_api", "away_api"]
        header_list.extend(self.feature_list)
        header_list.append(self.target_class_name)

        df1 = pd.DataFrame(data, columns=header_list)

        df1 = df1.iloc[np.random.permutation(len(df1))]
        self.feature_list = new_feature_list
        # split the data to train and test data with the given split percentage
        df_train, df_test = model_selection.train_test_split(df1, test_size=testing_split, random_state=42)

        # self.train_x = df_train.as_matrix(self.feature_list)
        temp = df_train.as_matrix(self.feature_list)
        # self.train_x = [float(i) for i in temp]

        self.train_x = [[float(j) for j in i] for i in temp]

        # self.test_x = df_test.as_matrix(self.feature_list)
        temp1 = df_test.as_matrix(self.feature_list)
        # self.test_x = [float(i) for i in temp1]
        self.test_x = [[float(j) for j in i] for i in temp1]

        # target class
        target_class = self.target_class_name

        mod_train_y = df_train.as_matrix([target_class])
        mod_test_y = df_test.as_matrix([target_class])

        # self.train_y = [i[0] for i in mod_train_y]
        # tempty = [i[0] for i in mod_train_y]
        self.train_y = [self.conti(i) for i in mod_train_y]

        # self.test_y = [i[0] for i in mod_test_y]
        # temptxy = [i[0] for i in mod_test_y]
        self.test_y = [self.conti(i) for i in mod_test_y]



    def displayTree(self,clf):
        """
        displays tree graph with the trained graph.
        :param clf: 
        :return: 
        """
        new_feature_list = ["home_attack", "home_defence", "home_mid", "away_attack", "away_defence", "away_mid",
                                 "home_last_clash_wins",
                                 "away_last_clash_wins",
                                 "last_clash_draw"]
        dot_data = StringIO()
        tree.export_graphviz(clf, out_file=dot_data,
        feature_names=new_feature_list,
        class_names=["home_win", "draw", "away_win"],
        filled=True, rounded=True,
        special_characters=True)
        graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
        graph.write_pdf("tree2.pdf")



    def train(self,testing_split, test_data):
        """
        train is used to train the model using the training data created.
        :param testing_split: 
        :return: 
        """
        with open('english.csv', 'rb') as f:
            reader = csv.reader(f)
            train_data = list(reader)

        self.__format_input(train_data,testing_split)

        training_x = [test_data[i] for i in range(2, 11)]
        clf = tree.DecisionTreeClassifier()
        clf.fit(self.train_x, self.train_y)
        #self.displayTree(clf)
        # print self.test_x
        predicted_y = clf.predict(self.test_x)
        test_result= clf.predict(training_x)



        # FOR FEATURE IMPORTANCE.
        importances = clf.feature_importances_

        feature_array = np.asanyarray(self.train_x)
        names = ["home_attack", "home_defence", "home_mid", "away_attack", "away_defence", "away_mid",
                            "home_last_clash_wins",
                            "away_last_clash_wins",
                            "last_clash_draw"]
        # display_imp_plot(clf,names)
        cnf_matrix = confusion_matrix(self.test_y, predicted_y)
        np.set_printoptions(precision=2)

        # Plot non-normalized confusion matrix

        class_names = ["win", "draw", "lose"]
        # plot_confusion_matrix(cnf_matrix, classes=class_names,
        #                       title='Confusion matrix, decision tree')

        print "Accuracy for Decision Tree:"
        print accuracy_score(self.test_y,predicted_y)

        print("result for test_row:")
        print test_result





