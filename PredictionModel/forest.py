from sklearn.ensemble import RandomForestClassifier
from BaseModel.model import BaseModel
from sklearn.metrics import accuracy_score
import csv
from sklearn import tree,model_selection
import pandas as pd
import numpy as np
from Explore.explore import *

import matplotlib.pyplot as plt
from Explore.explore import display_importance

from sklearn.metrics import confusion_matrix

class RandForest(BaseModel):
    """
    RandForest model uses random forest for predicting the target class.
    """

    def __int__(self):
        BaseModel.__init__(self)

    def conti(self, x):
        if x == '1':
            return "home_win"
        elif x == '2':
            return "draw"
        else:
            return "away_win"

    def __format_input(self, data, testing_split):
        """
        It will format the given data into dataframe and split them into training and testing
        data as per the split ratio given.
        :param data:
        :param testing_split:
        :return:
        """
        new_feature_list = ["home_attack", "home_defence", "home_mid", "away_attack", "away_defence", "away_mid",
                            "home_last_clash_wins",
                            "away_last_clash_wins",
                            "last_clash_draw"]
        # here the data is changed to pandas dataframe. dataframe contains features list
        # well as the target class
        header_list = ["home_api", "away_api"]
        header_list.extend(self.feature_list)
        header_list.append(self.target_class_name)

        df1 = pd.DataFrame(data, columns=header_list)

        df1 = df1.iloc[np.random.permutation(len(df1))]
        self.feature_list = new_feature_list
        # split the data to train and test data with the given split percentage
        df_train, df_test = model_selection.train_test_split(df1, test_size=testing_split, random_state=42)

        # self.train_x = df_train.as_matrix(self.feature_list)
        temp = df_train.as_matrix(self.feature_list)
        # self.train_x = [float(i) for i in temp]

        self.train_x = [[float(j) for j in i] for i in temp]
        print self.train_x

        # self.test_x = df_test.as_matrix(self.feature_list)
        temp1 = df_test.as_matrix(self.feature_list)
        # self.test_x = [float(i) for i in temp1]
        self.test_x = [[float(j) for j in i] for i in temp1]

        # target class
        target_class = self.target_class_name

        mod_train_y = df_train.as_matrix([target_class])
        mod_test_y = df_test.as_matrix([target_class])

        # self.train_y = [i[0] for i in mod_train_y]
        # tempty = [i[0] for i in mod_train_y]
        self.train_y = [self.conti(i) for i in mod_train_y]

        # self.test_y = [i[0] for i in mod_test_y]
        # temptxy = [i[0] for i in mod_test_y]
        self.test_y = [self.conti(i) for i in mod_test_y]




    def train(self,testing_split,test_data):
        """
        trains the random forest model with created training data.
        :param testing_split: 
        :return: 
        """

        with open('english.csv', 'rb') as f:
            reader = csv.reader(f)
            train_data = list(reader)

        self.__format_input(train_data, testing_split)
        # get_correlaion(self.train_x)

        training_x = [test_data[i] for i in range(2, 11)]
        rf = RandomForestClassifier()
        rf.fit(self.train_x,self.train_y)
        predicted_y = rf.predict(self.test_x)
        importances = rf.feature_importances_

        feature_array = np.asanyarray(self.train_x)
        display_importance(feature_array,importances,rf)

        cnf_matrix = confusion_matrix(self.test_y, predicted_y)
        np.set_printoptions(precision=2)
        test_result = rf.predict(training_x)


        # Plot non-normalized confusion matrix

        class_names = ["win", "draw", "lose"]
        # plot_confusion_matrix(cnf_matrix, classes=class_names,
        #                       title='Confusion matrix,Random Forest')

        print "Accuracy for Random Forest:"
        print accuracy_score(self.test_y, predicted_y)

        print("Result for test_row:")
        print test_result


