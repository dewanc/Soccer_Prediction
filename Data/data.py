"""
data.py contains all the classes for data like Match, Player, Team etc.
It only contains the related data to this entity and encloses them in class.
"""
from Util.db import DbHelper

class Match:
    """
    Match contains all the data to a specific match.
    It is intitialized with a row of match from the Matcbes table
    """
    def __init__(self, match_row):
        self.match_row = match_row
        self.set_values()

    def __get_value(self, col_name):
        """
        __get_value will rerurn the specified column value from the dict row.
        :param col_name: 
        :return: 
        """
        return self.match_row[col_name]

    def __get_players_dict(self, string_var):
        """
        It will return a dictionary of players either home or away.
        :param string_var: 
        :return: dictionary of players with the postion coordinates as the key.
        """
        # team string is either home or away.
        team_string = string_var

        players_dict ={}
        for i in range(1,12):
            # base_column_string eg. home_player_
            base_column_string = team_string+"_player_"

            # create the x-coordinate string
            x_cor_string = base_column_string+"X"+str(i)

            # create the y-coordinate string
            y_cor_string = base_column_string+"Y"+str(i)

            # create the player string
            player_string = base_column_string+str(i)

            # create the tuple of X and Y coordinate values.
            cor_tuple = (self.match_row[x_cor_string], self.match_row[y_cor_string])

            # create the dict for player such that the coordinate are the key for the dict. eg (X,Y) => player
            players_dict[cor_tuple] = self.match_row[player_string]


        return players_dict

    def set_values(self):
        self.league_id = self.__get_value("league_id")
        self.date = self.__get_value("date")
        self.id = self.__get_value("id")
        self.season = self.__get_value("season")
        self.goal = self.__get_value("goal")
        self.possession = self.__get_value("possession")

        # get all home team data
        self.home_team_api = self.__get_value("home_team_api_id")
        self.home_team_goal = self.__get_value("home_team_goal")
        self.home_player_dict = self.__get_players_dict("home")

        # get all away team data
        self.away_team_api = self.__get_value("away_team_api_id")
        self.away_team_goal = self.__get_value("away_team_goal")
        self.away_player_dict = self.__get_players_dict("away")




class Player:
    """
    Player class contains all the information about the specific player.
    Player object can be initialize with the player_api_id.
    """
    def __init__(self, player_api_id):
        self.id = player_api_id
        self.set_values()

    def set_values(self):
        # fetch the player attribures row from the Player_Attributes table
        player_attributes_row = DbHelper.get_player_attributes(self.id)

        # set the overall rating for this player.
        self.overall_rating = player_attributes_row[0]['overall_rating']

        # TO DO: overall rating is used for now but as other features are needed we will add the attributes as per the
        # need