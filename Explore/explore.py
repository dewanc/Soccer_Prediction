
import itertools
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from sklearn import svm, datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import csv
import plotly.plotly as py
import plotly.graph_objs as go

def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    sns.set(font_scale=2.5)
    plt.figure()
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="red" if cm[i, j] > thresh else "black",fontsize = 25)

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()






def display_importance(X,importances,forest):
        std = np.std([tree.feature_importances_ for tree in forest.estimators_],
                     axis=0)
        indices = np.argsort(importances)[::-1]
        print indices

        # ditonary for feature.
        feature_dict = {0:"home_attack",1: "home_defence",2: "home_mid",3: "away_attack",4: "away_defence",5: "away_mid",
                           6: "home_clash_wins",
                            7:"away_clash_wins",
                            8: "last_clash_draw"}


        feature_sorted_list =[feature_dict[i] for i in indices]
        # Print the feature ranking
        print("Feature ranking:")
        sns.set(font_scale=1.6)
        for f in range(X.shape[1]):
            print("%d. feature %d (%f)" % (f + 1, indices[f], importances[indices[f]]))

        # Plot the feature importances of the forest
        plt.figure()
        plt.title("Feature importances")
        plt.bar(range(X.shape[1]), importances[indices],
                color="r", yerr=std[indices], align="center")
        plt.xticks(range(X.shape[1]), feature_sorted_list)
        plt.xlim([-1, X.shape[1]])
        plt.show()


# def display_imp_plot(rfc,names):
#     importance = rfc.feature_importances_
#
#     # Sort the feature importances
#     sorted_importances = np.argsort(importance)
#
#     # Insert padding
#     padding = np.arange(len(names) - 1) + 0.5
#
#     # Plot the data
#     plt.barh(padding, importance[sorted_importances], align='center')
#
#     # Customize the plot
#     plt.yticks(padding, names[sorted_importances])
#     plt.xlabel("Relative Importance")
#     plt.title("Variable Importance")
#
#     # Show the plot
#     plt.show()

# def get_correlaion(data_list):
#     names = ["home_attack", "home_defence", "home_mid", "away_attack", "away_defence", "away_mid",
#                             "home_last_clash_wins",
#                             "away_last_clash_wins",
#                             "last_clash_draw"]
#     # Generate a large random dataset
#     # rs = np.random.RandomState(33)
#     d = pd.DataFrame(data=data_list,
#                      columns=names)
#
#     # Compute the correlation matrix
#     corr = d.corr()
#     print corr
#
#     # Generate a mask for the upper triangle
#     mask = np.zeros_like(corr, dtype=np.bool)
#     mask[np.triu_indices_from(mask)] = True
#     sns.set(font_scale=1.7)
#     # Set up the matplotlib figure
#     f, ax = plt.subplots(figsize=(11, 9))
#
#     # Generate a custom diverging colormap
#     cmap = sns.diverging_palette(220, 10, as_cmap=True)
#
#     # Draw the heatmap with the mask and correct aspect ratio
#
#     ax.set_xticklabels(corr.columns.values, rotation=90)
#
#
#     sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3,
#                 square=True, xticklabels=corr.columns.values, yticklabels=corr.columns.values,
#                 linewidths=.5, cbar_kws={"shrink": .5}, ax=ax)
#
#     # sns.heatmap(corr, mask=np.zeros_like(corr, dtype=np.bool), cmap=sns.diverging_palette(220, 10, as_cmap=True),
#     #             square=True, ax=ax)
#     plt.yticks(rotation=0)
#     plt.show()
#     sns.set(font_scale=1.6)





def get_box_plot(data, title,indx):
    point_list =[]
    for lst in data:
        point_list.append(lst[indx])

    plist = np.asarray(point_list)
    point_list.append(10)
    rc("lines", markeredgewidth=0.5)
    plt.suptitle(title, fontsize=20)
    plt.boxplot(plist, 0, 'gD')
    plt.show()

def display_box_plot():
    with open('english.csv', 'rb') as f:
        reader = csv.reader(f)
        train_data = list(reader)

    feature_list = [
        "home_attack", "home_defence", "home_mid", "away_attack", "away_defence", "away_mid",
        "home_last_wins", "home_last_draws",
        "away_last_wins", "away_last_draws",
        "home_last_clash_wins",
        "away_last_clash_wins",
        "last_clash_draw"
    ]
    target_class_name = "target_class"
    new_feature_list = ["home_attack", "home_defence", "home_mid", "away_attack", "away_defence", "away_mid",
                        "home_last_clash_wins",
                        "away_last_clash_wins",
                        "last_clash_draw"]
    # here the data is changed to pandas dataframe. dataframe contains features list
    # well as the target class
    header_list = ["home_api", "away_api"]
    header_list.extend(feature_list)
    header_list.append(target_class_name)

    df1 = pd.DataFrame(train_data, columns=header_list)

    df1 = df1.iloc[np.random.permutation(len(df1))]
    feature_list = new_feature_list

    # self.train_x = df_train.as_matrix(self.feature_list)
    temp = df1.as_matrix(feature_list)
    # self.train_x = [float(i) for i in temp]

    train_x = [[float(j) for j in i] for i in temp]

    print train_x

    get_box_plot(train_x,"home_attack",0)
    get_box_plot(train_x, "home_defence", 1)
    get_box_plot(train_x, "home_mid", 2)
    get_box_plot(train_x, "away_attack", 3)
    get_box_plot(train_x, "away_defence", 4)
    get_box_plot(train_x, "away_mid", 5)
    get_box_plot(train_x, "home_last_clash_wins", 6)
    get_box_plot(train_x, "away_last_clash_wins", 7)
    get_box_plot(train_x, "last_clash_draw", 8)



