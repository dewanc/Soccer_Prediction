"""
model.py contains the base model class the define all the functionality need for a base model class.
"""
from Util.db import DbHelper
from Data.data import Match
from Data.data import Player
import csv



class BaseModel:
    """
    BaseModel define all the methods needed for a generic Machine learning model.
    """

    def __init__(self):

        # set the features name
        self.feature_list = [
                             "home_attack", "home_defence", "home_mid", "away_attack", "away_defence", "away_mid",
                             "home_last_wins", "home_last_draws",
                             "away_last_wins", "away_last_draws",
                             "home_last_clash_wins",
                             "away_last_clash_wins",
                             "last_clash_draw"
                             ]
        self.target_class_name = "target_class"
        self.features_num = len(self.feature_list)

    def get_last_wins(self,team_api, match_date, scenario):
        """
        get_last_wins gives number of win for this particular team in last 10 games in that scenario.
        :param team_api: 
        :param date: 
        :return: 
        """
        match_rows = DbHelper.get_matches(team_api, match_date, scenario)
        win_count =0
        draw_count =0
        for row in match_rows:
            match = Match(row)
            if scenario ==1 :
                # home
                goals = match.home_team_goal - match.away_team_goal
            else:
                # away
                goals = match.away_team_goal - match.home_team_goal

            if goals > 0:
                win_count +=1
            elif goals == 0:
                draw_count +=1
            else:
                pass

        return [win_count, draw_count]


    def get_last_results(self, home_id, away_id, match_date):
        """
        get_last_results gives all the wins of the past match between the two teams.
        :param home_id: 
        :param away_id: 
        :param match_id: 
        :return: 
        """
        home_wins =0
        away_wins =0
        draws =0
        matches = DbHelper.get_last_results(home_id, away_id, match_date)
        for row in matches:
            match = Match(row)
            if match.home_team_goal > match.away_team_goal:
                home_wins+=1
            elif match.home_team_goal< match.away_team_goal:
                away_wins+=1
            else:
                draws +=1

        results ={}
        results['home'] = home_wins
        results['away'] = away_wins
        results['draw'] = draws
        return results


    def get_training_features(self,  match):
        """
        get_training_features generates all the features from both team and returns as a list.
        
        :param match: Match object of a game.
        :return: list of features for a game.
        """
        home_attributes_list = self.get_team_attributes(match.home_player_dict)
        home_attack = home_attributes_list['attack']
        home_mid = home_attributes_list['mid']
        home_defence = home_attributes_list['defence']

        away_attributes_list = self.get_team_attributes(match.away_player_dict)
        away_attack = away_attributes_list['attack']
        away_mid = away_attributes_list['mid']
        away_defence = away_attributes_list['defence']


        home=1
        away =2

        home_win_draw = self.get_last_wins(match.home_team_api, match.date, home )
        home_last_wins = home_win_draw[0]
        home_last_draws = home_win_draw[1]

        away_win_draw = self.get_last_wins(match.away_team_api, match.date, away)
        away_last_wins =  away_win_draw[0]
        away_last_draws =away_win_draw[1]




        last_clashes = self.get_last_results(match.home_team_api, match.away_team_api, match.date)
        home_last_clash_wins = last_clashes['home']
        away_last_clash_wins = last_clashes['away']
        last_clash_draw = last_clashes['draw']


        # find the win lose or draw i.e target class.
        target_class = None

        goals = match.home_team_goal - match.away_team_goal
        if goals > 0:
            target_class = home_win = 1
        elif goals == 0:
            target_class = draw = 2
        else:
            target_class = away_win = 3

        training_data = [match.home_team_api, match.away_team_api,
                        home_attack, home_defence, home_mid, away_attack, away_defence, away_mid,
                         home_last_wins, home_last_draws,
                         away_last_wins, away_last_draws,
                         home_last_clash_wins,
                         away_last_clash_wins,
                         last_clash_draw, target_class]
        return training_data


    def __calculate_sum_att(self, att_type, player_dict):
        """
        calculate_sum_att takes the coordinat to calculate the attributes for a team.
        :param coordinate_list: 
        :return: 
        """

        # notations used: (*,10) => forwards , (*,7) => mid, (*,3) => defence, (1,1) = GK
        if att_type == 1:
            xcoor_list = [4,6]
            ycoor = 10
        elif att_type == 2:
            xcoor_list = [2,4,6,8]
            ycoor = 7
        else:
            xcoor_list = [2,4,6,8]
            ycoor = 3

        valid_player_count = 1
        att_value = 0
        for i in xcoor_list:
            if not (i,ycoor) in player_dict:
                continue
            player_id = player_dict[(i, ycoor)]
            if player_id is None:
                continue
            player = Player(player_id)
            #
            att_value += player.overall_rating
            #att_value += over
            valid_player_count += 1

        # adding goal keeper.
        if att_type == 3:
            if (0,0) in player_dict:
                player_id  = player_dict[(0, 0)]
                if player_id is not None:
                    player = Player(player_id)
                    att_value += player.overall_rating
                    #over = DbHelper.get_player_attributes(player_id)[0]['overall_rating']
                    att_value += player.overall_rating
                    #att_value += over
                    valid_player_count +=1
        att_value = att_value/valid_player_count
        return att_value




    def get_team_attributes(self, player_dict):
        """
        get_team_attributes generate the attack, mid, defence for the specified team.
        :param player_dict: dict to player in each postion.
        :return: dict to attack, mid, defence of the team.
        """

        attribute_dict = {}
        # get the attack attrib
        attack =1
        mid =2
        defence =3

        attribute_dict['attack'] = self.__calculate_sum_att(attack, player_dict)
        attribute_dict['mid'] = self.__calculate_sum_att(mid, player_dict)
        attribute_dict['defence'] = self.__calculate_sum_att(defence, player_dict)
        return attribute_dict




    def get_training_data(self):
        """
        get_training_data creates training data from the Database and returns a list of feature and target class.
        :return: List of training rows.
        """


        temp_counter =0
        final_training_data = []
        matches = DbHelper.get_all_matches()
        for row in matches:
            # create a Match object for feature creation.
            match = Match(row)

            # extract all the features from both team.
            temp_training_data = self.get_training_features(match)
            with open("english.csv", 'a') as myfile:
                wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
                wr.writerow(temp_training_data)
            #final_training_data.append(temp_training_data)
            print(temp_counter)
            temp_counter+=1
        #return final_training_data


    def train(self, testing_split):
        raise NotImplementedError