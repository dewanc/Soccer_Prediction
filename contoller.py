from PredictionModel.neuralnet import NeuralNet
from PredictionModel.dtree import DecisionTree
from PredictionModel.forest import RandForest
from sklearn.datasets import load_iris
from sklearn import tree
from Explore.explore import *
import csv



# test_data = [8203, 9987, 43, 55, 51, 46, 54, 54, 2, 6, 6, 0, 1, 0, 0, 1]

with open('test.csv', 'rb') as f:
    reader = csv.reader(f)
    test_data = list(reader)

test_data =test_data[0]


# display_box_plot()

model = NeuralNet()
model.train(0.2)
model.predict_match(test_data)



model = DecisionTree()
model.train(0.2, test_data)

model = RandForest()
model.train(0.2, test_data)