                                   EUROPEAN FOOTBALL RESULT PREDICTION


REQUIREMENTS:
	* Python 3.0 or above.
	* Scikit learn
	* Numpy
	* Pandas
	* Tensorflow 1.1
	* matplotlib

INSTALATION:
	> linux based OS
		* Run the installation script present in project home directory.
		* sh dependency.sh
	> Windows
		* Install all the above requirements.

Project Structure:
	> Soccer Prediction
		> Utility Package
			# This package has all methods for database access abstraction.
			> db.py

		> Data Package
			# This package contains all data only related classes: player, match
			> data.py

		> BaseModel Package
			# This package contains generic base model implementation.
			> model.py

		> Model Package
			# This package contains specific model class like tree, random forest, neural net.
			> dtree.py
			> randomForest.py
			> neural.py

		> Explore Package
			# This package contains methods for exploratary analysis.
			> explore.py

		> Controller.py
			# This python file is the main controller point of the project.

HOW TO RUN:
	* Install all dependencies by running the dependency.sh
	* Uncomment model that is to be run.
	* Run the controller.py from home directory.
	* Explore.py contains all the methods for exploratory analysis.

LICENSE:
	Everything in this project is under MIT License and is free to modify and
	experiment unless the author is mentioned. Any commercial distribuion is 
	not allowed.

