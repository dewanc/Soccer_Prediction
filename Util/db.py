import sqlite3
# tables : [(u'sqlite_sequence',), (u'Player_Attributes',), (u'Player',)
# (u'Match',), (u'League',), (u'Country',), (u'Team',), (u'Team_Attributes',)]

class DbHelper():

    path_db = "/home/dewan/PycharmProjects/Soccer_Prediction/Util/database.sqlite"
    cursor = None
    where_clause_string = "home_player_1 is NOT NULL and home_player_X1 is NOT NULL and home_player_Y1 is NOT NULL and away_player_1 is NOT NULL and   away_player_X1 is NOT NULL and away_player_Y1 is NOT NULL and home_player_2 is NOT NULL and home_player_X2 is NOT NULL and home_player_Y2 is NOT NULL and away_player_2 is NOT NULL and   away_player_X2 is NOT NULL and away_player_Y2 is NOT NULL and home_player_3 is NOT NULL and home_player_X3 is NOT NULL and home_player_Y3 is NOT NULL and away_player_3 is NOT NULL and   away_player_X3 is NOT NULL and away_player_Y3 is NOT NULL and home_player_4 is NOT NULL and home_player_X4 is NOT NULL and home_player_Y4 is NOT NULL and away_player_4 is NOT NULL and   away_player_X4 is NOT NULL and away_player_Y4 is NOT NULL and home_player_5 is NOT NULL and home_player_X5 is NOT NULL and home_player_Y5 is NOT NULL and away_player_5 is NOT NULL and   away_player_X5 is NOT NULL and away_player_Y5 is NOT NULL and home_player_6 is NOT NULL and home_player_X6 is NOT NULL and home_player_Y6 is NOT NULL and away_player_6 is NOT NULL and   away_player_X6 is NOT NULL and away_player_Y6 is NOT NULL and home_player_7 is NOT NULL and home_player_X7 is NOT NULL and home_player_Y7 is NOT NULL and away_player_7 is NOT NULL and   away_player_X7 is NOT NULL and away_player_Y7 is NOT NULL and home_player_8 is NOT NULL and home_player_X8 is NOT NULL and home_player_Y8 is NOT NULL and away_player_8 is NOT NULL and   away_player_X8 is NOT NULL and away_player_Y8 is NOT NULL and home_player_9 is NOT NULL and home_player_X9 is NOT NULL and home_player_Y9 is NOT NULL and away_player_9 is NOT NULL and   away_player_X9 is NOT NULL and away_player_Y9 is NOT NULL and home_player_10 is NOT NULL and home_player_X10 is NOT NULL and home_player_Y10 is NOT NULL and away_player_10 is NOT NULL and   away_player_X10 is NOT NULL and away_player_Y10 is NOT NULL and home_player_11 is NOT NULL and home_player_X11 is NOT NULL and home_player_Y11 is NOT NULL and away_player_11 is NOT NULL and   away_player_X11 is NOT NULL and away_player_Y11 is NOT NULL "


    @classmethod
    def __get_map(cls, lists):
        column_name_list = [description[0] for description in cls.cursor.description]
        map_list = []
        for row in lists:
            new_map = {}
            for i, val in zip(column_name_list, row):
                new_map[i] = val
            map_list.append(new_map)
        return map_list


    @classmethod
    def get_all_tables(cls):
        """
        get_all_tables returns all tables present in the database.
        :return: List of tables present
        """
        conn = sqlite3.connect(cls.path_db)
        cls.cursor = conn.cursor()
        cursor = cls.cursor
        cursor.execute("SELECT name FROM sqlite_master WHERE type='table'")
        tables = cursor.fetchall()
        return tables

    @classmethod
    def __read(cls,table_name, col_name , kv_map, limit=None, clause=" and "):
        """
        get_all_matches returns all matches from match table
        :return: list of mathces
        """
        if table_name is None:
            return None

        columns_name =""
        for col in col_name:

            if col == col_name[0]:
                columns_name = columns_name+ col
            else:
                columns_name = columns_name +","+col

        if not kv_map:
            where_clause = ""
        else:
            where_clause = " where "
            flag =0
            for key in kv_map:
                if flag ==1:
                    where_clause = where_clause+ clause
                where_clause = where_clause+ str(key)+ " = " +str(kv_map[key])
                flag =1

        if limit == None:
            limit_str =""
        else:
            limit_str=" LIMIT " + str(limit)

        cls.db_connect()
        cursor = cls.cursor
        query_str =" SELECT " + columns_name + " FROM " +table_name + where_clause + limit_str
        cursor.execute(query_str)
        matches = cursor.fetchall()
        map_list = cls.__get_map(matches)
        return map_list

    @classmethod
    def __query(cls, query_str):
        conn = sqlite3.connect(cls.path_db)
        cls.cursor = conn.cursor()
        cursor = cls.cursor
        cursor.execute(query_str)
        rows = cursor.fetchall()
        dict_list = cls.__get_map(rows)
        return dict_list

    @classmethod
    def get_all_matches(cls):
        """
        
        get_all_matches returns all matches from match table
        :return: list of mathces
        """


        query_str ="SELECT * FROM Match where %s and league_id =1729  "%(cls.where_clause_string)
        dict_list = cls.__query(query_str)

        # for i in dict_list:
        #     print(i)
        return  dict_list

    @classmethod
    def get_all_players(cls):
        """
        get_all_players return all the players from 
        :return: 
        """
        query_str = "SELECT * from player"
        dict_list = cls.__query(query_str)
        return dict_list

    @classmethod
    def get_all_team_attributes(cls):
        """
        get_all_team_attributes return all the team_attributes from team_attributes 
        :return: dict of team attributes
        """
        query_str = "SELECT * from Team_Attributes"
        dict_list = cls.__query(query_str)
        return dict_list

    @classmethod
    def get_player_attributes(cls, player_id):
        """
        get_player_attributes returns  the player_attributes from Player_Attributes of the specific player 
        :return: dict of player attributes
        """
        query_str = "SELECT * from Player_Attributes where player_api_id = %s"%(player_id)
        dict_list = cls.__query(query_str)
        return dict_list

    @classmethod
    def get_player(cls, player_id):
            """
            get_player_attributes returns  the player_attributes from Player_Attributes of the specific player 
            :return: dict of player attributes
            
            
            """
            query_str = "SELECT * from Player where player_api_id = %s" % (player_id)
            dict_list = cls.__query(query_str)
            return dict_list



            # @classmethod
    # def get_all_player(cls):
    #     """
    #     get_all_team_attributes return all the team_attributes from team_attributes
    #     :return: dict of team attributes
    #     """
    #     query_str = "SELECT * from Player"
    #     dict_list = cls.__query(query_str)
    #     return dict_list
    #
    # @classmethod
    # def get_all_team(cls):
    #     """
    #     get_all_team_attributes return all the team_attributes from team_attributes
    #     :return: dict of team attributes
    #     """
    #     query_str = "SELECT * from Team"
    #     dict_list = cls.__query(query_str)
    #     return dict_list



    @classmethod
    def get_matches(cls, team_id,match_date, scenario):
        """
        returns match rows with mathces of that particular team in particular scenario before 
        the specified date.
        :param team_id: 
        :param scenario: 
        :return: 
        """
        if scenario == 1:
            # home
            team_string = "home_team_api_id"

        else :
            #away
            team_string = "away_team_api_id"
        query_str = "SELECT * from Match where  %s = %s and date < '%s' order by date desc limit 10"% ( team_string,team_id, match_date)
        dict_list = cls.__query(query_str)
        return dict_list


    @classmethod
    def get_last_results(cls, home_id, away_id, match_date):
        """
        returns match row of last 10 matches where these two teams played
        :param home_id: 
        :param away_id: 
        :param match_date: 
        :return: 
        """

        query_string = "SELECT * from Match  where home_team_api_id in (%s, %s) and away_team_api_id in (%s, %s) and date < '%s' order by date desc limit 10"%(
                                                                                                                            home_id,away_id,home_id,away_id, match_date)
        dict_list = cls.__query(query_string)
        return dict_list




    @classmethod
    def get_match(cls,match_id):
        query_string = "SELECT * from Match where match_api_id = %s"%(match_id)
        dict_list =cls.__query(query_string)
        return dict_list


    @classmethod
    def get_leagues(cls):
        query = "select * from League"
        dict_list = cls.__query(query)
        return dict_list